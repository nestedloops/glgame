cmake_minimum_required(VERSION 2.6)
project(game)

find_package(GLUT)
find_package(OpenGL)

add_subdirectory(src)
